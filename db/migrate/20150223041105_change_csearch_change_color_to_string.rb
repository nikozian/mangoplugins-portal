class ChangeCsearchChangeColorToString < ActiveRecord::Migration
  def change
  	change_column :csearches, :color1, :string
  	change_column :csearches, :color2, :string
  	change_column :csearches, :background1, :string
  	change_column :csearches, :background2, :string
  end
end
