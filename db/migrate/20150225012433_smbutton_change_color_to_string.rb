class SmbuttonChangeColorToString < ActiveRecord::Migration
  def change
  	change_column :smbuttons, :color1, :string
  	change_column :smbuttons, :color2, :string
  	change_column :smbuttons, :background1, :string
  	change_column :smbuttons, :background2, :string
  end
end
