class CreateSmbuttons < ActiveRecord::Migration
	def change
		create_table :smbuttons do |t|
			t.integer :user_id
			t.integer :status
			t.integer :type
			t.integer :color1
			t.integer :color2
			t.integer :background1
			t.integer :background2
			t.integer :size
			t.string :url
			t.timestamps null: false
		end
	end
end
