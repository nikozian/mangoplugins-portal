class SmbuttonRemoveExtraColors < ActiveRecord::Migration
  def change
  	remove_column :smbuttons, :color2
  	remove_column :smbuttons, :background2
  	remove_column :smbuttons, :size
  end
end
