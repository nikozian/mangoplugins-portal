class Smbutton < ActiveRecord::Base
	validates :user_id, presence: true
	validates :status, presence: true
	validates :network, presence: true
	validates :color1, presence: true
	validates :background1, presence: true
	validates :url, presence: true

	mount_uploader :sound, SoundUploader

	belongs_to :user

	scope :active, -> {
		where("status == 1")
	}
	scope :inactive, -> {
		where("status == 0")
	}

	scope :facebook, -> {
		where("network = 1")
	}
	scope :twitter, -> {
		where("network = 2")
	}
	scope :instagram, -> {
		where("network = 3")
	}
	scope :youtube, -> {
		where("network = 4")
	}
end
