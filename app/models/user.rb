class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
	validates :fname, presence: true
	validates :lname, presence: true
	validates :email, presence: true
	validates :password, presence: true
	validates :status, presence: true
	validates :cycle, presence: true

	validates :email, uniqueness: true

	has_many :csearches
	has_many :smbuttons

	scope :paid, -> {
		where("status == 1")
	}
	scope :unpaid, -> {
		where("status == 0")
	}
	scope :cycle1, -> {
		where("cycle == 1")
	}
	scope :cycle2, -> {
		where("cycle == 2")
	}

	def to_s
		"#{fname} #{lname}"
	end
end
