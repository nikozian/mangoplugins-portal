class Csearch < ActiveRecord::Base
	validates :user_id, presence: true
	validates :status, presence: true
	validates :engine, presence: true
	validates :color1, presence: true
	validates :color2, presence: true
	validates :background1, presence: true
	validates :background2, presence: true
	validates :size, presence: true

	belongs_to :user

	scope :active, -> {
		where("status == 1")
	}
	scope :inactive, -> {
		where("status == 0")
	}
	scope :google, -> {
		where("engine == 1")
	}
	scope :yahoo, -> {
		where("engine == 2")
	}
	scope :bing, -> {
		where("engine == 3")
	}
	scope :mini, -> {
		where("size == 1")
	}
	scope :normal, -> {
		where("size == 2")
	}
end
