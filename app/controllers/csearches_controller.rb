class CsearchesController < ApplicationController
	
	before_filter :authenticate_user!

	def index
		@list = current_user.csearches
	end

	def show
		@csearch = Csearch.find(params[:id])
		respond_to do |format|
			format.json { render json: @csearch }
		end
	end

	def new
		@csearch = Csearch.new
	end

	def create
		@c = Csearch.new(params[:csearch].permit(:engine, :color1, :color2, :background1, :background2, :size))
		@c.user = current_user
		@c.status = 1
		@c.save
		redirect_to "/csearches"
	end
	def destroy
		@d = Csearch.find( params[:id] )
		@d.destroy
		redirect_to "/csearches"
	end

end
