class SmbuttonsController < ApplicationController

	before_filter :authenticate_user!

	def index
		@list = current_user.smbuttons
	end

	def show

		respond_to do |f|
			f.json {
				@smbutton = Smbutton.find(params[:id])

				if @smbutton.user != current_user
					render nothing: true
				else
					render json: @smbutton
				end

			}
			f.html {
				render nothing: true
			}
			f.xml {
				render nothing: true
			}
		end

	end

	def new
		@smbutton = Smbutton.new
	end

	def create
		@s = Smbutton.new( params[:smbutton].permit( :network, :color1, :background1, :text, :url, :sound ) )

		case @s.network
			when 1
				@s.url = "http://facebook.com/" + @s.url
			when 2
				@s.url = "http://twitter.com/" + @s.url
			when 3
				@s.url = "http://instagram.com/" + @s.url
			when 4
				@s.url = "http://youtube.com/" + @s.url
			else
				@s.url = "http://google.com"
		end

		@s.user = current_user
		@s.status = 1
		@s.save
		redirect_to "/smbuttons"
	end

	def destroy
		@d = Smbutton.find( params[:id] )
		@d.destroy
		redirect_to "/smbuttons"
	end

end