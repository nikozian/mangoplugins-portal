module ApplicationHelper

	def userBox
		str = "<div class='user-box'>"

		if user_signed_in?
			str += "<p id='user-info'>Hi #{current_user}! ( #{link_to('Logout', destroy_user_session_path, :method => :delete)}  )"
		else
			str += "<p id='user-info'>Hi guest!"
		end
		str += "</div>"
		raw(str)
	end
end
