# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )

Rails.application.config.assets.precompile += %w( users.css )
Rails.application.config.assets.precompile += %w( users.js )

Rails.application.config.assets.precompile += %w( registrations.css )
Rails.application.config.assets.precompile += %w( registrations.js )

Rails.application.config.assets.precompile += %w( csearches.css )
Rails.application.config.assets.precompile += %w( csearches.js )

Rails.application.config.assets.precompile += %w( smbuttons.css )
Rails.application.config.assets.precompile += %w( smbuttons.js )
Rails.application.config.assets.precompile += %w( smbuttons-new.js )